<!DOCTYPE html>
<html>
<head>
    <title>Form Input Mahasiswa</title>
    <link rel="stylesheet" type="text/css" href="desain.css">
</head>
<body>

<div class="box">
    <h1>Form Input Nilai Mahasiswa</h1>
    <form method="post" action="login.php">
        <div class="box-label">
            <label for="nim">NIM:</label>
            <input type="text" id="nim" name="nim">
        </div>
        <div class="box-label">
            <label for="nama">Nama:</label>
            <input type="text" id="nama" name="nama">
        </div>
        <div class="box-label">
            <label for="umur">Umur:</label>
            <input type="text" id="umur" name="umur">
        </div>
        <div class="box-label">
            <label for="prodi">Program Studi:</label>
            <select name="prodi">
                <option value="">Select...</option>
                <option value="Informatika">Informatika</option>
                <option value="Teknik Industri">Teknik Industri</option>
                <option value="Teknik Elektro">Teknik Elektro</option>
                <option value="Teknik Kimia">Teknik Kimia</option>
                <option value="Teknologi Pangan">Teknologi Pangan</option>
            </select>
        </div>
        <div class="box-label">
            <label for="nilai">Nilai:</label>
            <input type="text" id="nilai" name="nilai">
        </div>
        <div class="box-label">
            <label>Jenis Kelamin:</label>
            <div class="box-input">
                <input type="radio" name="gender" value="Laki-laki"> Laki-laki
                <input type="radio" name="gender" value="Perempuan"> Perempuan
            </div>
        </div>
        <div class="box-label">
            <label for="status">Status:</label>
            <select name="status">
                <option value="">Select...</option>
                <option value="aktif">Aktif</option>
                <option value="tidak aktif">Tidak Aktif</option>
            </select>
        </div>
        <input class="submit" type="submit" name="submit" value="Submit">
        <input class="submit" type="reset" name="reset" value="Reset">
    </form>
</div>

</body>
</html>
