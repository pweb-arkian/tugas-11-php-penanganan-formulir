<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nim = $nama = $umur = $prodi = $nilai = $gender = $status = "";
    $nimErr = $namaErr = $umurErr = $prodiErr = $nilaiErr = $genderErr = $statusErr = "";
    $isValid = true;

    if (empty($_POST["nim"])) {
        $nimErr = '<span style="color: red;">NIM masih kosong, harus diisi!</span>';
        $isValid = false;
    } elseif (!is_numeric($_POST["nim"])) {
        $nimErr = '<span style="color: red;">NIM harus berupa angka!</span>';
        $isValid = false;
    } else {
        $nim = $_POST["nim"];
    }

    if (empty($_POST["nama"])) {
        $namaErr = '<span style="color: red;">Nama masih kosong, harus diisi!</span>';
        $isValid = false;
    } else {
        $nama = $_POST["nama"];
    }

    if (empty($_POST["umur"])) {
        $umurErr = '<span style="color: red;">Umur masih kosong, harus diisi!</span>';
        $isValid = false;
    } elseif (!is_numeric($_POST["umur"])) {
        $umurErr = '<span style="color: red;">Umur harus berupa angka!</span>';
        $isValid = false;
    } else {
        $umur = $_POST["umur"];
    }

    if (empty($_POST["prodi"])) {
        $prodiErr = '<span style="color: red;">Prodi masih kosong, harus diisi!</span>';
        $isValid = false;
    } else {
        $prodi = $_POST["prodi"];
    }

    if (empty($_POST["gender"])) {
        $genderErr = '<span style="color: red;">Gender masih kosong, harus diisi!</span>';
        $isValid = false;
    } else {
        $gender = $_POST["gender"];
    }

    if (empty($_POST["nilai"])) {
        $nilaiErr = '<span style="color: red;">Nilai masih kosong, harus diisi!</span>';
        $isValid = false;
    } elseif (!is_numeric($_POST["nilai"])) {
        $nilaiErr = '<span style="color: red;">Nilai harus berupa angka!</span>';
        $isValid = false;
    } else {
        $nilai = $_POST["nilai"];
    }

    if (empty($_POST["status"])) {
        $statusErr = '<span style="color: red;">Status masih kosong, harus diisi!</span>';
        $isValid = false;
    } else {
        $status = $_POST["status"];
    }

    if ($isValid) {
        echo "NIM : " . $nim . "<br>";
        echo "Nama : " . $nama . "<br>";
        echo "Umur : " . $umur . "<br>";
        echo "Program Studi: " . $prodi . "<br>";
        echo "Gender : " . $gender . "<br>";

        if ($nilai > 80) {
            echo "Nilai: $nilai (A) , Lulus." . "<br>";
        } elseif ($nilai > 70 && $nilai <= 80) {
            echo "Nilai: $nilai (B) , Lulus." . "<br>";
        } elseif ($nilai > 60 && $nilai <= 70) {
            echo "Nilai: $nilai (C) , Lulus" . "<br>";
        } elseif ($nilai > 50 && $nilai <= 60) {
            echo "Nilai : $nilai (D) , Tidak Lulus" . "<br>";
        } else {
            echo "Nilai : $nilai (E) , Tidak Lulus" . "<br>";
        }

        if ($status == "aktif") {
            echo "Status : Aktif" . "<br>";
        } else {
            echo "Status : Tidak Aktif" . "<br>";
        }
    } else {
        echo "Ada kesalahan dari data yang telah kamu inputkan:<br>";
        echo "NIM: " . $nimErr . "<br>";
        echo "Nama: " . $namaErr . "<br>";
        echo "Umur: " . $umurErr . "<br>";
        echo "Prodi: " . $prodiErr . "<br>";
        echo "Gender: " . $genderErr . "<br>";
        echo "Nilai: " . $nilaiErr . "<br>";
        echo "Status: " . $statusErr . "<br>";
    }
}
?>
